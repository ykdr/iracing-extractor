import "./config/config.mjs";
import cliProgress from "cli-progress";
import colors from "ansi-colors";
import iRacingService from "./services/iRacingService.mjs";
const iRacingServiceInstance = new iRacingService();
import ExcelService from "./services/ExcelService.mjs";
const ExcelServiceInstance = new ExcelService();
import CsvUtils from "./utils/CsvUtils.mjs";
import fs from "fs";

// Create new progress bar
const progressBar = new cliProgress.SingleBar({
  format: `{task} |${colors.cyan("{bar}")}| {percentage}%`,
  barCompleteChar: "\u2588",
  barIncompleteChar: "\u2591",
  hideCursor: true,
});

// Start progress
progressBar.start(100, 0, {
  task: "Logging in iRacing API",
});

// Login to iRacing API
await iRacingServiceInstance.login();

// Update progress
progressBar.update(10, {
  task: "Loading drivers",
});

// Split drivers into array
const driversId = process.env.DRIVERS.split(" ");
let counter = 1;
const drivers = [];

// Load all drivers' data
for (let driverId of driversId) {
  const progress = 10 + (counter * 40) / driversId.length;
  try {
    const driver = await iRacingServiceInstance.getDriverInfo(driverId);
    progressBar.update(progress, { task: `Processing driver ${counter}/${driversId.length}` });

    drivers.push(driver.toCsv());
    counter++;
  } catch (e) {
    console.log(`\n${e.message}`);
    process.exit();
  }
}

// Read results JSON
const rawdata = fs.readFileSync("./input/series.json");
const data = JSON.parse(rawdata);
const series = [];

let driversToProcess = 0;
data.series.forEach((serie) => serie.carClasses.forEach((carClass) => (driversToProcess += carClass.drivers.length)));

for (let s of data.series) {
  const serie = await iRacingServiceInstance.getSerieInfo(s.name, data.year, data.quarter, s.carClasses);

  for (const carClass of s.carClasses) {
    counter = 1;
    for (const driverId of carClass.drivers) {
      const progress = 50 + (counter * 20) / driversToProcess;
      progressBar.update(progress, { task: `Processing serie ${s.name} - driver's result ${counter}/${carClass.drivers.length}` });
      await iRacingServiceInstance.processDriverSerieResults(driverId, serie, carClass.name);

      counter++;
    }
  }

  series.push(serie);
}

// Convert to CSV
progressBar.update(80, { task: "Converting drivers' info to CSV" });
await CsvUtils.toCsv(drivers);

// Convert to Excel
progressBar.update(90, { task: "Converting results to Excel" });
await ExcelServiceInstance.toExcel(series);

// Job finished
progressBar.update(100, { task: "Done !" });
progressBar.stop();
