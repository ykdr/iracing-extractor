import ExcelJS from "exceljs";
const categoryColors = ["f94144", "f3722c", "f8961e", "f9844a", "f9c74f", "90be6d", "43aa8b", "4d908e", "577590", "277da1"];

class ExcelUtils {
  initWorkbook() {
    this.workbook = new ExcelJS.Workbook({ useSharedStrings: true, useStyles: true });
  }

  addWorksheet(name) {
    name = name.substring(0, 31);
    this.actualWorksheet = this.workbook.addWorksheet(name, { views: [{ showGridLines: false }] });
  }

  initMainHeader(serieName, year, quarter, racesLength) {
    this.actualWorksheet.mergeCells(2, 2, 2, racesLength + 3);
    const serieCell = this.actualWorksheet.getCell(2, 2);
    serieCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieCell.font = { bold: true, size: 16 };
    serieCell.alignment = { horizontal: "center" };
    serieCell.value = serieName;

    this.actualWorksheet.mergeCells(2, racesLength + 4, 2, racesLength + 9);
    const seasonCell = this.actualWorksheet.getCell(2, racesLength + 4);
    seasonCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    seasonCell.font = { bold: true, size: 16 };
    seasonCell.alignment = { horizontal: "center" };
    seasonCell.value = `${year} Season ${quarter}`;

    this.actualWorksheet.getColumn(2).width = 30;

    for (let i = 0; i < racesLength + 6; i++) {
      this.actualWorksheet.getColumn(i + 4).width = 15;
    }
  }

  fillRacesHeader(races) {
    // Filler
    this.actualWorksheet.mergeCells(4, 2, 4, 3);
    this.actualWorksheet.getCell(4, 2).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    // Add dates' header
    const racesDatesHeader = this.actualWorksheet.getCell(5, 2);
    racesDatesHeader.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
    };
    racesDatesHeader.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    racesDatesHeader.value = "Fin semaine";

    // Fill races
    let raceColumn = 4;
    for (const race of races) {
      const raceCell = this.actualWorksheet.getCell(4, raceColumn);
      raceCell.border = {
        top: { style: "medium" },
        left: { style: "medium" },
        bottom: { style: "thin" },
        right: { style: "medium" },
      };
      raceCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
      raceCell.value = `W${race.week} \r\n${race.trackName}`;

      const raceDateCell = this.actualWorksheet.getCell(5, raceColumn);
      raceDateCell.border = {
        top: { style: "medium" },
        left: { style: "medium" },
        bottom: { style: "thin" },
        right: { style: "medium" },
      };
      raceDateCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
      const startDate = new Date(race.startDate);
      const endDate = new Date(startDate);
      endDate.setDate(endDate.getDate() + 6);
      const formattedDate = endDate.toLocaleDateString("fr-FR", { day: "2-digit", month: "2-digit", year: "2-digit" });
      raceDateCell.value = formattedDate;

      const today = new Date();
      const isBetweenStartAndEnd = today >= startDate && today <= endDate;
      const isEndDatePassed = today > endDate;

      if (isBetweenStartAndEnd || isEndDatePassed) {
        raceDateCell.font = { bold: true };
      }

      if (isBetweenStartAndEnd) {
        raceDateCell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "ffc6efce" },
          bgColor: { argb: "ff000000" },
        };
      }

      if (isEndDatePassed) {
        raceDateCell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "ffffc7ce" },
          bgColor: { argb: "ff000000" },
        };
      }

      raceColumn++;
    }

    // Rest of races' row
    const champPointsCell = this.actualWorksheet.getCell(4, raceColumn);
    champPointsCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champPointsCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champPointsCell.font = { bold: true };
    champPointsCell.value = "TOTAL \r\npoints";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    const champClubDivRankCell = this.actualWorksheet.getCell(4, ++raceColumn);
    champClubDivRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champClubDivRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champClubDivRankCell.font = { bold: true };
    champClubDivRankCell.value = "Classement Club Division";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    const champClubRankCell = this.actualWorksheet.getCell(4, ++raceColumn);
    champClubRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champClubRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champClubRankCell.font = { bold: true };
    champClubRankCell.value = "Classement Club";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    const champDivisionRankCell = this.actualWorksheet.getCell(4, ++raceColumn);
    champDivisionRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champDivisionRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champDivisionRankCell.font = { bold: true };
    champDivisionRankCell.value = "Classement Division";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    const champRankCell = this.actualWorksheet.getCell(4, ++raceColumn);
    champRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champRankCell.font = { bold: true };
    champRankCell.value = "Classement";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };

    const champWeeksCell = this.actualWorksheet.getCell(4, ++raceColumn);
    champWeeksCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    champWeeksCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    champWeeksCell.font = { bold: true };
    champWeeksCell.value = "Nombre \r\nManches";
    // Date line
    this.actualWorksheet.getCell(5, raceColumn).border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
  }

  addCategory(categoryName, categoryColorIndex, rowIndex, racesLength) {
    // Add category line
    this.actualWorksheet.mergeCells(rowIndex, 2, rowIndex, racesLength + 9);
    const categoryCell = this.actualWorksheet.getCell(rowIndex, 2);
    categoryCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    categoryCell.font = { bold: true };
    categoryCell.fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: categoryColors[categoryColorIndex % categoryColors.length] },
      bgColor: { argb: categoryColors[categoryColorIndex % categoryColors.length] },
    };
    categoryCell.value = categoryName;
    categoryCell.name = "category";
  }

  addDriver(driver, division, rowIndex) {
    this.actualWorksheet.mergeCells(rowIndex, 2, rowIndex + 2, 2);
    const driverCell = this.actualWorksheet.getCell(rowIndex, 2);
    driverCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    driverCell.alignment = { vertical: "middle", horizontal: "center" };
    if (division) {
      driverCell.value = `${driver.displayName} \r\nDivision ${division}`;
    } else {
      driverCell.value = `${driver.displayName}`;
    }

    const positionCell = this.actualWorksheet.getCell(rowIndex, 3);
    positionCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "thin" },
      right: { style: "medium" },
    };
    positionCell.alignment = { vertical: "middle", horizontal: "center" };
    positionCell.value = "Position";

    const sofCell = this.actualWorksheet.getCell(rowIndex + 1, 3);
    sofCell.border = {
      top: { style: "thin" },
      left: { style: "medium" },
      bottom: { style: "thin" },
      right: { style: "medium" },
    };
    sofCell.alignment = { vertical: "middle", horizontal: "center" };
    sofCell.value = "SOF";

    const pointsCell = this.actualWorksheet.getCell(rowIndex + 2, 3);
    pointsCell.border = {
      top: { style: "thin" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    pointsCell.alignment = { vertical: "middle", horizontal: "center" };
    pointsCell.value = "Points";
  }

  fillRacesResults(racesResults, rowIndex) {
    let raceIndex = 0;
    let racesPoints = racesResults.map((raceResult) => raceResult.points);
    racesPoints.filter((points) => points != null).sort((a, b) => a - b);
    for (const raceResult of racesResults) {
      const positionRaceCell = this.actualWorksheet.getCell(rowIndex, raceIndex + 4);
      positionRaceCell.border = {
        top: { style: "medium" },
        left: { style: "medium" },
        bottom: { style: "thin" },
        right: { style: "medium" },
      };
      positionRaceCell.alignment = { vertical: "middle", horizontal: "center" };
      positionRaceCell.value = raceResult.finishPosition;

      const sofRaceCell = this.actualWorksheet.getCell(rowIndex + 1, raceIndex + 4);
      sofRaceCell.border = {
        top: { style: "thin" },
        left: { style: "medium" },
        bottom: { style: "thin" },
        right: { style: "medium" },
      };
      sofRaceCell.alignment = { vertical: "middle", horizontal: "center" };
      sofRaceCell.value = raceResult.sof;

      const pointsRaceCell = this.actualWorksheet.getCell(rowIndex + 2, raceIndex + 4);
      pointsRaceCell.border = {
        top: { style: "thin" },
        left: { style: "medium" },
        bottom: { style: "medium" },
        right: { style: "medium" },
      };
      pointsRaceCell.alignment = { vertical: "middle", horizontal: "center" };
      pointsRaceCell.value = raceResult.points;

      if (racesPoints.indexOf(pointsRaceCell.value) < 8 && pointsRaceCell.value != null) {
        pointsRaceCell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "ffc6efce" },
          bgColor: { argb: "ff000000" },
        };
      } else {
        pointsRaceCell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "ffffc7ce" },
          bgColor: { argb: "ff000000" },
        };
      }

      raceIndex++;
    }
  }

  fillSerieResults(results, rowIndex, racesLength) {
    // Series points
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 4, rowIndex + 2, racesLength + 4);
    const seriePointsCell = this.actualWorksheet.getCell(rowIndex, racesLength + 4);
    seriePointsCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    seriePointsCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    seriePointsCell.value = results.championshipPoints;

    // Series rank division + club
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 5, rowIndex + 2, racesLength + 5);
    const serieDivisionClubRankCell = this.actualWorksheet.getCell(rowIndex, racesLength + 5);
    serieDivisionClubRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieDivisionClubRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    serieDivisionClubRankCell.value = results.divisionClubRank;

    // Series rank club
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 6, rowIndex + 2, racesLength + 6);
    const serieClubRankCell = this.actualWorksheet.getCell(rowIndex, racesLength + 6);
    serieClubRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieClubRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    serieClubRankCell.value = results.clubRank;

    // Series rank division
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 7, rowIndex + 2, racesLength + 7);
    const serieDivisionRankCell = this.actualWorksheet.getCell(rowIndex, racesLength + 7);
    serieDivisionRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieDivisionRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    serieDivisionRankCell.value = results.divisionRank;

    // Series rank
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 8, rowIndex + 2, racesLength + 8);
    const serieRankCell = this.actualWorksheet.getCell(rowIndex, racesLength + 8);
    serieRankCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieRankCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    serieRankCell.value = results.rank;

    // Series weeks counted
    this.actualWorksheet.mergeCells(rowIndex, racesLength + 9, rowIndex + 2, racesLength + 9);
    const serieWeeksCell = this.actualWorksheet.getCell(rowIndex, racesLength + 9);
    serieWeeksCell.border = {
      top: { style: "medium" },
      left: { style: "medium" },
      bottom: { style: "medium" },
      right: { style: "medium" },
    };
    serieWeeksCell.alignment = { wrapText: true, vertical: "middle", horizontal: "center" };
    serieWeeksCell.value = results.weeksCounted;

    if (results.weeksCounted >= 8) {
      serieWeeksCell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "ffc6efce" },
        bgColor: { argb: "ff000000" },
      };
    }
  }

  async toDisk(filename) {
    await this.workbook.xlsx.writeFile(filename);
  }
}

export default ExcelUtils;
