import ObjectsToCsv from "objects-to-csv";
import fs from "fs";
import moment from "moment";
const folder = "./res";

class CsvUtils {
  static async toCsv(object) {
    // Create res folder if not existing
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder);
    }

    const csv = new ObjectsToCsv(object);
    const filename = moment().format("DD-MM-yyyy");
    return await csv.toDisk(`${folder}/${filename}.csv`);
  }
}

export default CsvUtils;
