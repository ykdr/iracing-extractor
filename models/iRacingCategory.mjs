const iRacingCategory = {
  OVAL: 1,
  ROAD: 2,
  DIRT_OVAL: 3,
  DIRT_ROAD: 4,
  SPORTS_CAR: 5,
  FORMULA_CAR: 6,
};

export default iRacingCategory;
