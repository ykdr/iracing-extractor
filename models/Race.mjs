class Race {
  constructor() {
    this.week = null;
    this.trackId = null;
    this.trackName = null;
    this.configName = null;
    this.startDate = null;
  }
}

export default Race;
