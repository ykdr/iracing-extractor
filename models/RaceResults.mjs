class RaceResults {
  constructor() {
    this.finishPosition = null;
    this.sof = null;
    this.points = null;
    this.week = null;
  }
}

export default RaceResults;
