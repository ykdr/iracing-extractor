class Driver {
  constructor() {
    this.id = null;
    this.displayName = null;
    this.sportsCarRating = null;
    this.formulaCarRating = null;
    this.sportsCarRatingDate = null;
    this.formulaCarRatingDate = null;
    this.sportsCarSafetyRating = null;
    this.formulaCarSafetyRating = null;
  }

  toCsv() {
    return {
      Pilotes: this.displayName,
      "iRating Sports Car": this.sportsCarRating,
      "Safety Rating Sports Car": this.sportsCarSafetyRating,
      "Date iRating Sports Car": this.sportsCarRatingDate,
      "iRating Formula Car": this.formulaCarRating,
      "Safety Rating Formula Car": this.formulaCarSafetyRating,
      "Date iRating Formula Car": this.formulaCarRatingDate,
      "ID pilote": this.id,
    };
  }
}

export default Driver;
