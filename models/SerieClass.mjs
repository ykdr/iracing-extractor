class SerieClass {
  constructor() {
    this.id = null;
    this.name = null;
    this.results = [];
  }
}

export default SerieClass;
