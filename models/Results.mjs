class Results {
  constructor() {
    this.driver = null;
    this.championshipPoints = null;
    this.weeksCounted = null;
    this.rank = null;
    this.divisionRank = null;
    this.clubRank = null;
    this.divisionClubRank = null;
    this.racesResults = [];
    this.division = null;
  }
}

export default Results;
