class Serie {
  constructor() {
    this.id = null;
    this.seasonId = null;
    this.year = null;
    this.quarter = null;
    this.name = null;
    this.races = [];
    this.classes = [];
  }
}

export default Serie;
