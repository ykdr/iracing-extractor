import CryptoJS from "crypto-js";
import axios from "axios";
import { wrapper } from "axios-cookiejar-support";
import { CookieJar } from "tough-cookie";
import Driver from "../models/Driver.mjs";
import iRacingCategory from "../models/iRacingCategory.mjs";
import Serie from "../models/Serie.mjs";
import Race from "../models/Race.mjs";
import Results from "../models/Results.mjs";
import RaceResults from "../models/RaceResults.mjs";
import SerieClass from "../models/SerieClass.mjs";

class iRacingService {
  constructor() {
    const jar = new CookieJar();
    this.client = wrapper(axios.create({ jar }));
  }

  async login() {
    const hash = CryptoJS.SHA256(process.env.API_PASSWORD + process.env.API_USER.toLowerCase());
    const base64Hash = CryptoJS.enc.Base64.stringify(hash);

    await this.client.post("https://members-ng.iracing.com/auth", { email: process.env.API_USER, password: base64Hash });
    await this.loadStaticData();
  }

  async loadStaticData() {
    this.series = await this.getSeries();
    this.seasons = await this.getSeasons();
  }

  async getDriverInfo(id) {
    const driver = new Driver();
    const driverData = await this.callApi(`https://members-ng.iracing.com/data/member/get?cust_ids=${id}`);

    if (driverData?.success) {
      driver.id = id;
      driver.displayName = driverData.members[0].display_name;

      const sportsCarRatingData = await this.getDriverRating(id, iRacingCategory.SPORTS_CAR);

      if (sportsCarRatingData?.success) {
        const lastRatingData = sportsCarRatingData.data[sportsCarRatingData.data.length - 1];

        if (lastRatingData != null) {
          driver.sportsCarRating = lastRatingData.value;
          driver.sportsCarRatingDate = lastRatingData.when;
        }
      }

      const formulaCarRatingData = await this.getDriverRating(id, iRacingCategory.FORMULA_CAR);

      if (formulaCarRatingData?.success) {
        const lastRatingData = formulaCarRatingData.data[formulaCarRatingData.data.length - 1];

        if (lastRatingData != null) {
          driver.formulaCarRating = lastRatingData.value;
          driver.formulaCarRatingDate = lastRatingData.when;
        }
      }

      const sportsCarSafetyData = await this.getDriverSafetyRating(id, iRacingCategory.SPORTS_CAR);

      if (sportsCarSafetyData?.success) {
        const lastSafetyRatingData = sportsCarSafetyData.data[sportsCarSafetyData.data.length - 1];
        if (lastSafetyRatingData != null) {
          let classSafetyRating = this.getClassSafetyRating(lastSafetyRatingData.value);
          let safetyRating = this.getSafetyRating(lastSafetyRatingData.value);
          driver.sportsCarSafetyRating = `${classSafetyRating} ${safetyRating}`;
        }
      }

      const formulaCarSafetyData = await this.getDriverSafetyRating(id, iRacingCategory.FORMULA_CAR);

      if (formulaCarSafetyData?.success) {
        const lastSafetyRatingData = formulaCarSafetyData.data[formulaCarSafetyData.data.length - 1];
        if (lastSafetyRatingData != null) {
          let classSafetyRating = this.getClassSafetyRating(lastSafetyRatingData.value);
          let safetyRating = this.getSafetyRating(lastSafetyRatingData.value);
          driver.formulaCarSafetyRating = `${classSafetyRating} ${safetyRating}`;
        }
      }

      return driver;
    } else {
      return null;
    }
  }

  getClassSafetyRating(rating) {
    let classSafety = "";

    if (rating < 2000) {
      classSafety = "R";
    } else if ((rating >= 2000) & (rating < 3000)) {
      classSafety = "D";
    } else if ((rating >= 3000) & (rating < 4000)) {
      classSafety = "C";
    } else if ((rating >= 4000) & (rating < 5000)) {
      classSafety = "B";
    } else if (rating >= 5000) {
      classSafety = "A";
    }

    return classSafety;
  }

  getSafetyRating(rating) {
    let nbSafety = 0;

    if (rating < 2000) {
      nbSafety = (rating - 1000) / 100;
    } else if ((rating >= 2000) & (rating < 3000)) {
      nbSafety = (rating - 2000) / 100;
    } else if ((rating >= 3000) & (rating < 4000)) {
      nbSafety = (rating - 3000) / 100;
    } else if ((rating >= 4000) & (rating < 5000)) {
      nbSafety = (rating - 4000) / 100;
    } else if (rating >= 5000) {
      nbSafety = (rating - 5000) / 100;
    }

    return nbSafety;
  }

  async getSerieInfo(name, year, quarter, carClasses) {
    const serie = new Serie();
    serie.name = name;
    serie.year = year;
    serie.quarter = quarter;

    const serieData = this.getSeasonSerie(name, year, quarter);
    serie.id = serieData.series_id;
    serie.seasonId = serieData.season_id;

    const season = this.seasons.find((s) => s.season_id == serie.seasonId);

    for (const raceWeek of serieData.race_weeks) {
      const race = new Race();
      race.week = raceWeek.race_week_num + 1;
      race.trackId = raceWeek.track.track_id;
      race.trackName = raceWeek.track.track_name;
      race.configName = raceWeek.track.config_name;

      // Extend race's data
      if (season != null) {
        const seasonRace = season.schedules.find((s) => s.race_week_num == raceWeek.race_week_num);
        if (seasonRace != null) {
          race.startDate = new Date(seasonRace.start_date);
        }
      }

      serie.races.push(race);
    }

    for (const carClass of serieData.car_classes.filter((c) => carClasses.find((c2) => c2.name == c.name) != null)) {
      const serieClass = new SerieClass();
      serieClass.id = carClass.car_class_id;
      serieClass.name = carClass.name;

      serie.classes.push(serieClass);
    }

    return serie;
  }

  async processDriverSerieResults(driverId, serie, carClassName) {
    const results = new Results();
    results.driver = await this.getDriverInfo(driverId);
    const serieClass = serie.classes.find((c) => c.name == carClassName);

    const serieData = await this.getSeasonStandings(serie.seasonId, serieClass.id, driverId);

    if (serieData != null) {
      results.championshipPoints = serieData.points;
      results.weeksCounted = serieData.weeks_counted;
      results.rank = serieData.rank;
      results.division = serieData.division + 1;
      results.divisionRank = serieData.divisionRank;
      results.clubRank = serieData.clubRank;
      results.divisionClubRank = serieData.divisionClubRank;
    }

    for (let race of serie.races) {
      const raceResults = new RaceResults();
      raceResults.week = race.week;

      const raceData = await this.getDriverSerieWeekResults(driverId, serie.id, serie.year, serie.quarter, race.week - 1, carClassName);

      if (raceData != null) {
        raceResults.finishPosition = raceData.finish_position_in_class + 1;
        raceResults.sof = await this.getSubSessionSof(raceData.subsession_id, serieClass.id);
        raceResults.points = raceData.champ_points;
      }

      results.racesResults.push(raceResults);
    }

    serieClass.results.push(results);
  }

  async getDriverRating(id, category) {
    return await this.callApi(`https://members-ng.iracing.com/data/member/chart_data?cust_id=${id}&category_id=${category}&chart_type=1`);
  }

  async getDriverSafetyRating(id, category) {
    return await this.callApi(`https://members-ng.iracing.com/data/member/chart_data?cust_id=${id}&category_id=${category}&chart_type=3`);
  }

  async getSubSessionSof(subsessionId, carClassId) {
    const results = await this.callApi(`https://members-ng.iracing.com/data/results/get?subsession_id=${subsessionId}`);
    const raceResults = results.session_results.find((s) => s.simsession_name == "RACE").results;
    const ratings = raceResults.filter((r) => r.car_class_id == carClassId).map((r) => r.oldi_rating);
    const fixedSof = 1600 / Math.log(2);
    const sofExp = ratings.map((r) => Math.exp(-r / fixedSof));
    const sumSofExp = sofExp.reduce((a, b) => a + b, 0);
    const nbDrivers = ratings.length;
    const calculatedSof = fixedSof * Math.log(nbDrivers / sumSofExp);
    return Math.round(calculatedSof || 0);
  }

  // Returns best result (champ_points) for specific week
  async getDriverSerieWeekResults(driverId, serieId, year, quarter, week, carClassName) {
    let results = await this.callApi(
      `https://members-ng.iracing.com/data/results/search_series?cust_id=${driverId}&season_year=${year}&season_quarter=${quarter}&official_only=true&series_id=${serieId}`
    );

    results = results.filter((r) => r.event_type_name == "Race" && r.race_week_num == week && r.car_class_name == carClassName);

    const champPoints = this.getSerieWeekRacePoints(results);

    // if results = [] -> not raced during this week
    if (results.length == 0) {
      return null;
    }

    const result = results.reduce((p, c) => (p.champ_points > c.champ_points ? p : c));
    result.champ_points = champPoints;

    return result;
  }

  getSerieWeekRacePoints(results) {
    switch (results.length) {
      case 0:
        return 0;
      case 1:
      case 2:
      case 3:
      case 4:
        return results.reduce((p, c) => (p.champ_points > c.champ_points ? p : c)).champ_points;
      case 5:
      case 6:
      case 7:
      case 8:
        return this.getSerieWeekRacePointsAvg(results, 2);
      case 9:
      case 10:
      case 11:
      case 12:
        return this.getSerieWeekRacePointsAvg(results, 3);
      default:
        return this.getSerieWeekRacePointsAvg(results, 4);
    }
  }

  getSerieWeekRacePointsAvg(results, number) {
    const bestResults = results.sort((p, c) => c.champ_points - p.champ_points).slice(0, number);
    const sum = bestResults.map((r) => r.champ_points).reduce((a, b) => a + b, 0);
    return sum / bestResults.length || 0;
  }

  async getSeasonStandings(seasonId, carClassId, driverId) {
    const standings = await this.callApi(`https://members-ng.iracing.com/data/stats/season_driver_standings?season_id=${seasonId}&car_class_id=${carClassId}`);

    if (standings == null) {
      return null;
    } else {
      const res = standings.find((d) => d.cust_id == driverId);

      if (res == null) {
        return null;
      }

      res.divisionRank = standings.filter((d) => d.division == res.division).findIndex((d) => d.cust_id == driverId) + 1;
      res.clubRank = standings.filter((d) => d.club_id == res.club_id).findIndex((d) => d.cust_id == driverId) + 1;
      res.divisionClubRank = standings.filter((d) => d.division == res.division && d.club_id == res.club_id).findIndex((d) => d.cust_id == driverId) + 1;

      return res;
    }
  }

  getSeasonSerie(name, year, quarter) {
    const serie = this.series.find((s) => s.series_name == name);

    return serie.seasons.find((s) => s.season_year == year && s.season_quarter == quarter);
  }

  async getSeries() {
    return await this.callApi(`https://members-ng.iracing.com/data/series/stats_series`);
  }

  async getSeasons() {
    return await this.callApi(`https://members-ng.iracing.com/data/series/seasons`);
  }

  async callApi(url) {
    try {
      const res = await this.client.get(url);

      if (res.status == 200) {
        return res.data.link
          ? await this.getData(res.data.link)
          : await this.getChunk(res.data.data.chunk_info.base_download_url, res.data.data.chunk_info.chunk_file_names);
      }
    } catch (e) {
      console.log(e);
      throw new Error(`Error: ${e.response.data.error ? e.response.data.error : e.response.data}`);
    }

    return null;
  }

  async getData(url) {
    const res = await this.client.get(url);

    return res.data.chunk_info ? await this.getChunk(res.data.chunk_info.base_download_url, res.data.chunk_info.chunk_file_names) : res.data;
  }

  async getChunk(baseUrl, chunks) {
    let data = [];

    for (let chunk of chunks) {
      const res2 = await this.client.get(`${baseUrl}${chunk}`);
      data = data.concat(res2.data);
    }
    return data;
  }
}

export default iRacingService;
