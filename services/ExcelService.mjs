import ExcelUtils from "../utils/ExcelUtils.mjs";
import moment from "moment";
import fs from "fs";
const folder = "./res";

const ExcelUtilsInstance = new ExcelUtils();

class ExcelService {
  async toExcel(series) {
    // Create res folder if not existing
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder);
    }

    const filename = `${moment().format("DD-MM-yyyy")}-results.xlsx`;
    ExcelUtilsInstance.initWorkbook();

    for (const serie of series) {
      let serieName = serie.name;
      if (serieName.toLowerCase().includes("fixed")) {
        serieName = `${serieName.slice(0, 27)} - F`;
      }
      ExcelUtilsInstance.addWorksheet(serieName);
      ExcelUtilsInstance.initMainHeader(serie.name, serie.year, serie.quarter, serie.races.length);
      ExcelUtilsInstance.fillRacesHeader(serie.races);

      let serieRow = 6;
      let serieColorIndex = 0;
      for (const serieClass of serie.classes) {
        ExcelUtilsInstance.addCategory(serieClass.name, serieColorIndex, serieRow, serie.races.length);

        let resultRow = serieRow + 1;
        for (const result of serieClass.results) {
          ExcelUtilsInstance.addDriver(result.driver, result.division, resultRow);
          ExcelUtilsInstance.fillRacesResults(result.racesResults, resultRow);
          ExcelUtilsInstance.fillSerieResults(result, resultRow, serie.races.length);

          resultRow += 3;
        }

        serieRow += serieClass.results.length * 3 + 1;
        serieColorIndex++;
      }
    }

    await ExcelUtilsInstance.toDisk(`${folder}/${filename}`);
  }
}

export default ExcelService;
