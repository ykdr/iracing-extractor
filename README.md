# iRacing Extractor

iRacing data extractor to get drivers' information and current rating.

## Getting started

- Download [Node.JS](https://nodejs.org/en/).
- Download [this project](https://gitlab.com/ykdr/iracing-extractor/-/archive/main/iracing-extractor-main.zip)
- Unzip the project in a location of your choice
- Copy the **.env.example** file to a **.env** one a fill it with:
  - API_USER: iRacing email
  - API_PASSWORD: iRacing password
  - DRIVERS: list of iRacing drivers' id **separated by a blank**
    - example: DRIVERS=XXX XXX XXX
- Open a Node.JS cmd by launching **Node.js command prompt**
- Go to the unzipped project location

```
cd C:\Users\your\project\location
```

- Node.JS modules installation

```
npm install
```

## Run the script

Each time you want to generate the file, simply run:

```
node app.mjs
```

The file will be generated under **res/_d-MM-yyyy_.csv**.

> :warning: The file will be overriden each time you generate it on the same day.
